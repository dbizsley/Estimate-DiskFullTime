#Script to monitor disk space and report estimated date until full.

<#
  Load all needed functios ready for the script processing.
#>
#define sample time between checking drive space.

Function Get-DiskInfo {
    $Disks = (Get-WmiObject win32_logicaldisk | ? {$_.Drivetype -eq 3})
    $Disks = $Disks | select DeviceID,@{"Name" = "Freespace";Expression ={'{0:0.0}' -f $_.Freespace / 1GB }},@{"Name" = "TotalSize";Expression ={'{0:0.0}' -f $_.Size /1GB}}
    Return $disks
}

function Get-EstimatedFullTime {
  param (
    $waittime = 600
  )
  $Output = @()
  Get-DiskInfo | % {
    $output += New-object psobject -property @{
      "Drive" = $_.DeviceID
      "PercentFree" = '{0:N2}' -f (($($_.Freespace) / $($_.TotalSize)) * 100)
    }
  }
  sleep $waittime

  $output2 = @()
  Get-DiskInfo | % {
    $output2 += New-object psobject -property @{
      "Drive" = $_.DeviceID
      "PercentFree" = '{0:N2}' -f (($($_.Freespace) / $($_.TotalSize)) * 100)
      "TimeUntilFull" = ""
    }
  }
  #cacluate rate and estimate time.
  foreach ($item in $output){
      $item2 = $Output2 | ? {$_.Drive -eq $item.Drive}
      $rate = ($item.PercentFree - $item2.PercentFree) / $waittime
      if ($rate -gt 0) {
          $estimatedfull = (100 - (100 -$item2.PercentFree)) / $rate
          $estimatedfullTime = New-timespan -seconds $estimatedfull
          ($output2 | ? {$_.Drive -like $item.Drive}).TimeUntilFull = $estimatedfullTime
        }
      }
    Return $output2
}
